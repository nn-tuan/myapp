import traceback

try:
    import os, sys
    from tkinter import *

    from View.MainView import MainView
    from View.SettingView import SettingView
    from View.DictionaryView import Dictionary_Search

    from ViewModel.MainViewModel import MainViewModel
    from ViewModel.SettingViewModel import SettingViewModel
    from ViewModel.ChangeStateEvent import ChangeEvent
    from ViewModel.ChangeStateEvent import MAINSTOPED, MAINSETTING, MAINSTARTING
    from ViewModel.DictionaryViewModel import DictionaryViewModel

    import Model.FileProcess as File

except:
    print(traceback.format_exc())
    input("Error!\nPress any key to exit!")



BUTTON1 = '<Button-1>'
SELECTBOX = '<<ComboboxSelected>>'
DELETE_WINDOW = "WM_DELETE_WINDOW"
KEYPRESS = "<KeyPress>"

try:
    pid = os.getpid()
    print(pid)
    path = os.getcwd()
except:
    print(traceback.format_exc())
    input("Press any key to ext!")


class MainModel():
    def __init__(self) -> None:
        self.print = False
        self.stateVM = ChangeEvent()
        pass

    # Hàm loop, kiểm tra trạng thái hoạt động của chương trình, trạng thái start - stop - setting
    def ChangeStateTimer(self, View=None, ViewModel=None):
        # Trạng thái Start, đang chạy Dictionary:
        if self.stateVM.State == MAINSTARTING:
            self.Dictionary.StartDictionary()
            pass
        # Trạng thái Stop, chưa chạy Dictionary:
        elif self.stateVM.State != MAINSTARTING:
            self.Dictionary.StopDictionary()
        # Trạng thái vào cài đặt, dừng chạy Dictionary:
        if self.stateVM.State == MAINSETTING:
            self.SettingWindow(mainView=View)
            ViewModel.GetFile2BoxItem(View)
            self.stateVM.State = MAINSTOPED
            pass
        self.Dictionary.print = self.print
        View.root.after(200, lambda : self.ChangeStateTimer(View=View, ViewModel=ViewModel))

    # Hàm khởi tạo màn hình Setting, binding cá
    def SettingWindow(self, mainView):
        settingView = SettingView(root=mainView.root, path=mainView.path)
        settingVM = SettingViewModel(path=path, stateVM=self.stateVM, file=File)
        try:
            getbox_bind_id     = settingView.Binding(settingView.list_item,     SELECTBOX, lambda event : settingVM.GetBoxItem(event, settingView))
            add_bind_id        = settingView.Binding(settingView.add_label,     BUTTON1, lambda event : settingVM.AddDictionary(event, mainView=settingView))
            edit_bind_id       = settingView.Binding(settingView.edit_label,    BUTTON1, lambda event : settingVM.EditDictionary(event, mainView=settingView))
            delete_bind_id     = settingView.Binding(settingView.delete_label,  BUTTON1, lambda event : settingVM.DeleteDictionary(event, mainView=settingView))
            save_bind_id       = settingView.Binding(settingView.save_label,    BUTTON1, lambda event : settingVM.SaveSetting(event, mainView=settingView))
            quit_bind_id       = settingView.Binding(settingView.quit_label,    BUTTON1, lambda event : settingVM.QuitWindow(event, mainView=settingView))
            text_bind_id       = settingView.Binding(settingView.Key_text,      KEYPRESS, lambda event : settingVM.CheckAnotherInputEvt(event, window=settingView))  

            check_bind_id      = settingView.Command(item=settingView.check_item_1, func=lambda : settingVM.GetCheckBox1(windows=settingView))
            check_bind_id      = settingView.Command(item=settingView.check_item_2, func=lambda : settingVM.GetCheckBox2(windows=settingView))
            check_bind_id      = settingView.Command(item=settingView.check_item_3, func=lambda : settingVM.GetCheckBox3(windows=settingView))

            settingVM.GetFile2BoxItem(windows=settingView)
            settingVM.AutoSetCheckBox(windows=settingView)
        except:
            print(traceback.format_exc())

        mainView.root.wait_window(settingView)

    # Hàm khởi tạo màn hình chính, binding các nút nhấn, lưu trạng thái chạy chương trình.
    def MainWindows(self):
        mainView        = MainView(path=path, stateVM=self.stateVM)
        mainViewModel   = MainViewModel(path=path, stateVM=self.stateVM)
        
        self.dictionaryView = Dictionary_Search()
        self.Dictionary     = DictionaryViewModel(master=self.dictionaryView, File=File)

        try:
            self.start_bind_id      = mainView.Binding(mainView.start_label,    BUTTON1,   lambda event : mainViewModel.StartCallBack(event, mainView=mainView))
            self.setting_bind_id    = mainView.Binding(mainView.setting_label,  BUTTON1,   lambda event : mainViewModel.Setting(event, mainView=mainView))
            self.hide_bind_id       = mainView.Binding(mainView.hide_label,     BUTTON1,   lambda event : mainViewModel.HideWindows(mainView=mainView))
            self.getbox_bind_id     = mainView.Binding(mainView.list_item,      SELECTBOX, lambda event : mainViewModel.GetBoxItem(event, mainView))

            mainViewModel.GetFile2BoxItem(mainView)

            mainView.root.protocol(DELETE_WINDOW, lambda : mainViewModel.DeleteWindowsCallback(mainView=mainView))
            mainView.root.after(1000, lambda : self.ChangeStateTimer(View=mainView, ViewModel=mainViewModel))
        except:
            print(traceback.format_exc())
        
        if True == 1:
            if mainViewModel.CheckRunID(mainView=mainView):
                return 0
        else:
            self.SettingWindow(mainView)

        File.file_print("RUNNING=TRUE", "RUNNING")
        File.file_print("ID=" + str(pid), "ID")
        mainView.loop()
        File.file_print("RUNNING=FALSE", "RUNNING")
        return 1

