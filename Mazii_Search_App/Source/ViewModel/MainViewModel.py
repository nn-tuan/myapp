"""




"""
import traceback


try:
    import os
    from pystray import MenuItem
    import pystray
    from PIL import Image
    from ViewModel.ChangeStateEvent import MAINSETTING, MAINSTARTING, MAINSTOPED
except:
    print(traceback.format_exc())
    pass

class MainViewModel():
    def __init__(self, path, stateVM) -> None:
        self.path =path
        self.stateVM = stateVM

        import sys
        sys.path.append(self.path + '\Model')
        import Model.FileProcess as FP
        self.FP = FP
        self.FP.set_os_path(os_path= self.path + "/Data/")

        self.hide_flag = False
        pass

    # Chương trình xử lý sự kiện nhất nút Start - Stop.
    def StartCallBack(self, event=None, mainView=None):
        try:
            if self.stateVM.State == MAINSTOPED:
                self.stateVM.State = MAINSTARTING
                mainView.start_label.config(bg='#ff704d', text="Stop")
            elif self.stateVM.State == MAINSTARTING:
                self.stateVM.State = MAINSTOPED
                mainView.start_label.config(bg='#5b9bd5', text="Start")
        except:
            print(traceback.format_exc())
        # print("test")
        pass

    # Xử lý khi có Event kết thúc chương trình:
    def DeleteWindowsCallback(self, mainView):
        try:
            aws = mainView.msg.askyesnocancel("Do you want to close the program?", "Click on the \"Yes\" button to close the program and click on the \"No\" button to hide the Program.")
            if aws != None:
                if aws:
                    self.QuitWindow(mainView=mainView)
                else:
                    self.HideWindows(mainView=mainView)
            pass
        except:
            print(traceback.format_exc())

    # Ẩn chương trình:
    def HideWindows(self, mainView):
        try:
            if self.stateVM.State != MAINSETTING:
                mainView.root.withdraw()
                image=Image.open(self.path + "\Data" + "\Icon\Dictionary_1.ico")
                menu= (
                        MenuItem('Show',    lambda event: self.ShowWindow(event, mainView=mainView)), 
                        MenuItem('Setting', lambda event : self.Setting(event, mainView=mainView)),
                        MenuItem('Quit',    lambda event: self.QuitWindow(event, mainView=mainView))
                    )
                icon=pystray.Icon("name", image, "DictionaryApp Menu", menu)
                self.hide_flag = True
                icon.run()
                self.hide_flag = False
        except:
            print(traceback.format_exc())

    # Hiển thị cài đặt:
    def Setting(self, event=None, mainView=None):
        try:
            self.ShowWindow(event=event, mainView=mainView)

            if self.stateVM.State < MAINSETTING:
                self.stateVM.State = MAINSETTING
                mainView.start_label.config(bg='#5b9bd5', text="Start")
        except:
            print(traceback.format_exc())
        pass

    # Hiển thị lại chương trình đã ẩn:
    def ShowWindow(self, event=None, mainView=None):
        try:
            if self.hide_flag:
                event.stop()
            mainView.root.deiconify()
        except:
            print(traceback.format_exc())
    
    # Nhận về giá trị của BoxItem:
    def GetBoxItem(self, event=None, windows=None):
        try:
            self.select_item = windows.select_item.get()
            print(self.select_item)
            
            data = self.FP.GetJson(self.FP.jsonPath)
            try:
                data['Select'] = int(self.select_item.split(":")[0])
                if self.stateVM.State == MAINSTARTING:
                    self.stateVM.State = MAINSTOPED
                    windows.start_label.config(bg='#5b9bd5', text="Start")
            except:
                data['Select'] = 0
            self.FP.SaveJson(path=self.FP.jsonPath, data=data)
        except:
            print(traceback.format_exc())

    # Ghi vào giá trị cho BoxItem:
    def SetBoxItem(self, item_value, windows=None):
        try:
            self.select_item = item_value
            windows.select_item.set(self.select_item)
        except:
            print(traceback.format_exc())
    
    # Set list giá trị cho BoxItem:
    def BoxItemSetList(self, item_list, windows=None):
        try:
            windows.list_item['values'] = item_list
        except:
            print(traceback.format_exc())
    
    # Hàm dùng để check lỗi File Json, nếu có sẽ tự fix về giá trị mặc định và trả về error = True
    def CheckJsonError(self, data):
        error = False
        try:
            if data['Select']:
                pass
        except:
            data['Select'] = 0
            error = True
        try: 
            if data['Key']:
                pass
        except:
            data['Key'] = "Key.shift"
            error = True

        try: 
            if data['Dictonary']:
                pass
        except:
            data['Dictonary'] = [{
                                    "Name": "Mazii",
                                    "website": "https://mazii.net/search/word?dict=javi&query=$value$&hl=vi-VN",
                                    "value": "$value$"
                                },]
            error = True
        return data, error

    # Hàm lấy giá trị list boxitem từ file .env
    def GetFile2BoxItem(self, windows=None):
        try:
            # Đọc file json:
            data = self.FP.GetJson(path=self.FP.jsonPath)
            data, error = self.CheckJsonError(data=data)
            if error:
                self.FP.SaveJson(path=self.FP.jsonPath, data=data)

            # Lấy thuộc tính Name để đưa vào danh sách trong BoxItem:
            item_show2_boxitem = []
            len_dic = len(data['Dictonary'])
            for index in range(0, len_dic):
                try:
                    item_show2_boxitem.append(str(index) + ": " + data['Dictonary'][index]['Name'])
                except:
                    item_show2_boxitem.append(str(index) + ": " + "No name")
            self.BoxItemSetList(item_show2_boxitem, windows)

            # Set Item hiển thị lên BoxItem. Số item lấy ở thuộc tính Select:
            self.SetBoxItem(str(data['Select']) + ":" + data['Dictonary'][data['Select']]['Name'], windows)
        except:
            print(traceback.format_exc())

    # Save data to json file        
    def SaveItem(self, data):
        try:
            self.FP.SaveJson(path=self.FP.jsonPath, data=data)
        except:
            print(traceback.format_exc())

    # Hàm kiểm tra chương trình đang chạy, nếu trả về FALSE => Dừng chương trình. 
    def CheckRun(self, mainView):
        pid = os.getpid()
        print("ID:", pid)
        if self.FP.get_data_file("RUNNING", self.FP.get_filepath() + "output.txt").upper() == "TRUE":
            print("TRUE")
        else:
            print("FALSE")
        mainView.root.after(1000, lambda : self.CheckRun(mainView))

    # Hàm kiểm tra có chương trình tương tự đang hoạt động trước đó không, nếu có sẽ đưa ra thông báo cho người dùng chọn lựa có tắt chương trình cũ hay là không.
    def CheckRunID(self, mainView):
        self.FP.set_os_path(self.path + "/Data/")
        if self.FP.get_data_file("RUNNING", self.FP.get_filepath() + "output.txt") == "TRUE":
            aws = mainView.msg.askyesno("Warning!", "There is a similar program running! Do you want to close the old program?\nIf you want to do it, please click on the \"Yes\" button to run this program.")
            if aws != None:
                if aws:
                    old_pid = self.FP.get_data_file("ID", self.FP.get_filepath() + "output.txt")
                    try:
                        os.kill(old_pid, 0)
                    except:
                        print(traceback.format_exc())
                else:
                    return 1
            pass
        return 0

    # Thoát chương trình:
    def QuitWindow(self, event=None, item=None, mainView=None):
        try:
            if self.hide_flag:
                event.stop()
            mainView.root.destroy()
        except:
            print(traceback.format_exc())
    