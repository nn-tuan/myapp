import traceback

try:
    import os
    from tkinter import NORMAL, DISABLED, Entry

    import Model.FileProcess as FP
    from Model.InputModel import Input
    from ViewModel.ChangeStateEvent import MAINSETTING, MAINSTARTING, MAINSTOPED
except:
    print(traceback.format_exc())
    pass

class SettingViewModel():
    def __init__(self, path=None, stateVM=None, file=FP) -> None:
        self.path =path
        self.stateVM = stateVM
        self.FP = FP
        self.Input = Input()
        self.Input.all_event_start = "KEY"
        self.Input.start()
        self.key_input = "None"

        self.FP.set_os_path(os_path= self.path + "/Data/")
        
        # Đọc file json:
        self.data = self.FP.GetJson(self.FP.jsonPath)

        self.data_select = 0
        pass

    # Nhận về giá trị của BoxItem:
    def GetBoxItem(self, event=None, windows=None):
        try:
            self.select_item = windows.select_item.get()
            
            print(self.select_item)
            
            try:
                self.data_select = int(self.select_item.split(":")[0])
                self.SetTextItem(data=self.data, index=self.data_select, windows=windows)
                # if self.stateVM.State == MAINSTARTING:
                #     self.stateVM.State = MAINSTOPED
                #     windows.start_label.config(bg='#5b9bd5', text="Start")
            except:
                self.data_select = 0
            # with open(self.FP.jsonPath, "w") as json_file:
            #     self.FP.dump(data, json_file)
            #     json_file.close()
        except:
            print(traceback.format_exc())

    # Sửa lại thông số trên TextBox:
    def SetTextItem(self, data, index, windows=None):
        try:
            windows.TextInsert(windows.Name_text,  data['Dictonary'][index]['Name'])
            windows.TextInsert(windows.Value_text, data['Dictonary'][index]['value'])
            windows.TextInsert(windows.URL_text,   data['Dictonary'][index]['website'])
        except:
            print(traceback.format_exc())
        pass

    # Ghi vào giá trị cho BoxItem:
    def SetBoxItem(self, item_value, windows=None):
        try:
            self.select_item = item_value
            windows.select_item.set(self.select_item)
        except:
            print(traceback.format_exc())
    
    # Set list giá trị cho BoxItem:
    def BoxItemSetList(self, item_list, windows=None):
        try:
            windows.list_item['values'] = item_list
        except:
            print(traceback.format_exc())
    
    # Hàm lấy giá trị list boxitem từ file cfg.txt
    def GetFile2BoxItem(self, windows=None):
        try:
            # Lấy thuộc tính Name để đưa vào danh sách trong BoxItem:
            item_show2_boxitem = []
            for index in range(len(self.data['Dictonary'])):
                try:
                    item_show2_boxitem.append(str(index) + ": " + self.data['Dictonary'][index]['Name'])
                except:
                    item_show2_boxitem.append(str(index) + ": " + "No name")
            self.BoxItemSetList(item_show2_boxitem, windows)

            # Set Item hiển thị lên BoxItem. Số item lấy ở thuộc tính Select:
            self.data_select = self.data['Select']
            self.SetBoxItem(str(self.data['Select']) + ":" + self.data['Dictonary'][self.data['Select']]['Name'], windows)
            self.SetTextItem(data=self.data, index=self.data_select, windows=windows)
        except:
            print(traceback.format_exc())

    # Save data to json file        
    def SaveItem(self, data):
        try:
            self.FP.SaveJson(path=self.FP.jsonPath, data=data)
        except:
            print(traceback.format_exc())

    # Hàm này dùng để tự động set giá trị của các checkbox theo dữ liệu trong file data khi được gọi.
    def AutoSetCheckBox(self, windows=None):
        if self.data['Key'] == "Key.ctrl_l":
            windows.check_var_1.set(1)
            windows.check_var_2.set(0)
            windows.check_var_2.set(0)
        elif self.data['Key'] == "Key.alt_l":
            windows.check_var_1.set(0)
            windows.check_var_2.set(1)
            windows.check_var_3.set(0)
        else:
            windows.check_var_1.set(0)
            windows.check_var_2.set(0)
            windows.check_var_3.set(1)
            self.key_input = self.data['Key']
            windows.Key_text.config(state=NORMAL)
            windows.after(100, lambda : self.run_after(window=windows))
        pass

    # Hàm xử lý nhập giá trị vào TextBox:
    def CheckAnotherInputEvt(self, event=None, window=None):
        self.key_input = self.Input.key_pressed

    def run_after(self, window):
        if window.check_var_3.get():
            window.TextInsert(window.Key_text, self.key_input)
            window.after(100, lambda : self.run_after(window=window))
        pass

    # Hàm xử lý checkbox chọn phím tắt khi chạy chương trình:
    def GetCheckBox1(self, event=None, windows=None):
        if windows.check_var_1.get():
            windows.check_var_2.set(0)
            windows.check_var_3.set(0)
        else:
            windows.check_var_2.set(1)
            windows.check_var_3.set(0)
        windows.TextInsert(windows.Key_text, "")
        windows.Key_text.config(state=DISABLED)
    
    # Hàm xử lý checkbox chọn phím tắt khi chạy chương trình:
    def GetCheckBox2(self, event=None, windows=None):
        if windows.check_var_2.get():
            windows.check_var_1.set(0)
            windows.check_var_3.set(0)
        else:
            windows.check_var_1.set(1)
            windows.check_var_3.set(0)
        windows.TextInsert(windows.Key_text, "")
        windows.Key_text.config(state=DISABLED)

    # Hàm xử lý checkbox chọn phím tắt khi chạy chương trình:
    def GetCheckBox3(self, event=None, windows=None):
        if windows.check_var_3.get():
            windows.check_var_1.set(0)
            windows.check_var_2.set(0)
        else:
            windows.check_var_1.set(1)
            windows.check_var_2.set(0)
        windows.Key_text.config(state=NORMAL)
        windows.after(100, lambda : self.run_after(window=windows))

    # Chương trình xử lý thêm Dictionary:
    def AddDictionary(self, event=None, mainView=None):
        try:
            diction_name  = mainView.GetText(mainView.Name_text)
            diction_value = mainView.GetText(mainView.Value_text)
            diction_url   = mainView.GetText(mainView.URL_text)

            data = { "Name":"", "website":"", "value":""}
            data["Name"]    = diction_name.replace("\n", "")
            data["website"] = diction_url.replace("\n", "")
            data["value"]   = diction_value.replace("\n", "")
            self.data['Dictonary'].append(data)
            
            aws = mainView.msg.showinfo("Successfully!", 
                                            "Name: " + data["Name"] + "\n" +  
                                            "Value: " + data["value"] + "\n" +
                                            "Website: " + data["website"] + "\n", 
                                        parent=mainView)
            self.data["Select"] = len(self.data['Dictonary']) - 1
            self.GetFile2BoxItem(windows=mainView)
            print(80 * "=")
            print(self.data['Dictonary'])
            pass
        except:
            try:
                aws = mainView.msg.showinfo("Error!",  traceback.format_exc(), parent=mainView)
            except:
                print(traceback.format_exc())
            
    # Chương trình xử lý sửa Dictionary:
    def EditDictionary(self, event=None, mainView=None):
        try:
            diction_name  = mainView.GetText(mainView.Name_text)
            diction_value = mainView.GetText(mainView.Value_text)
            diction_url   = mainView.GetText(mainView.URL_text)

            self.data['Dictonary'][self.data_select]["Name"]    = diction_name.replace("\n", "")
            self.data['Dictonary'][self.data_select]["website"] = diction_url.replace("\n", "")
            self.data['Dictonary'][self.data_select]["value"]   = diction_value.replace("\n", "")
            
            aws = mainView.msg.showinfo("Successfully!", 
                                            "Name: " + self.data['Dictonary'][self.data_select]["Name"] + "\n" +  
                                            "Value: " + self.data['Dictonary'][self.data_select]["value"] + "\n" +
                                            "Website: " + self.data['Dictonary'][self.data_select]["website"] + "\n", 
                                        parent=mainView)
            self.data["Select"] = self.data_select
            self.GetFile2BoxItem(windows=mainView)
            print(80 * "=")
            print(self.data['Dictonary'])
            pass
        except:
            try:
                aws = mainView.msg.showinfo("Error!",  traceback.format_exc(), parent=mainView)
            except:
                print(traceback.format_exc())
            
    # Chương trình xử lý xóa Dictionary:
    def DeleteDictionary(self, event=None, mainView=None):
        try:
            aws = mainView.msg.askyesno("Warning!","Do you want to delete this Dictionary?", parent=mainView)
            if aws == True:
                diction_name  = self.data['Dictonary'][self.data_select]["Name"]
                diction_value = self.data['Dictonary'][self.data_select]["value"]
                diction_url   = self.data['Dictonary'][self.data_select]["website"]
                self.data['Dictonary'].remove(self.data['Dictonary'][self.data_select])
                aws = mainView.msg.showinfo("Deleted!", 
                                                "Name: " + diction_name + "\n" +  
                                                "Value: " + diction_value + "\n" +
                                                "Website: " + diction_url + "\n", 
                                            parent=mainView)
                if self.data_select >= len(self.data['Dictonary']):
                    self.data["Select"] = len(self.data['Dictonary']) - 1
                else:
                    self.data["Select"] = self.data_select

                self.GetFile2BoxItem(windows=mainView)

            print(80 * "=")
            print(self.data['Dictonary'])
            pass
        except:
            try:
                aws = mainView.msg.showinfo("Error!",  traceback.format_exc(), parent=mainView)
            except:
                print(traceback.format_exc())
            
    # Chương trình xử lý lưu lại cài đặt:
    def SaveSetting(self, event=None, mainView=None):
        try:
            print("Save here")
            if mainView.check_var_1.get():
                self.data['Key'] = "Key.ctrl_l"
            elif mainView.check_var_2.get():
                self.data['Key'] = "Key.alt_l"
            else:
                if len(self.key_input) > 0 and self.key_input != "None":
                    self.data['Key'] = self.key_input
                pass

            self.SaveItem(data=self.data)
            self.Input.Key_Stop_Evnt()
            mainView.destroy()
            pass
        except:
            print(traceback.format_exc())

    # Thoát chương trình:
    def QuitWindow(self, event=None, mainView=None):
        try:
            self.Input.Key_Stop_Evnt()
            mainView.destroy()
        except:
            print(traceback.format_exc())