import traceback
try:
    import time
    import tkinter as TK
    from threading import Thread

    import pyautogui as pya
    import pyperclip

    from View.DictionaryView import Dictionary_Search
    import Model.FileProcess as File
    from Model.InputModel import Input
except:
    print(traceback.format_exc())
    pass

#===========================================================================================#
# Class khởi tạo chương trình chạy, điều khiển Dictionary:
#===========================================================================================#
class DictionaryViewModel(Thread):
    def __init__(self, master=None, key="Key.ctrl_l", File=File):
        Thread.__init__(self)
        self.FP = File
        self.key = key
        self.print = False
        self.dictionary = master

        self.runing = False
        self.check_key = False
        self.start_record_key = False
        self.check_mouse = False
        self.mouse_key_input = Input()
        self.mouse_key_input.all_event_start = "ALL"
        self.mouse_key_input.print = self.print
        self.time = 0
        self.time_key = 0

        self.position=(0, 0)
        self.start()
        pass

     # Hàm chuyển trạng thái của Dictionary thành Run, truyền thông số của thư viện đã lưu cho Dictionary.
    def Dictionary_Run(self):
        try:
            with open(self.FP.jsonPath, "r+") as json_file:
                data = self.FP.load(json_file)
                json_file.close()
            self.LoadUrl(data=data)

            self.dictionary.move_window(x=self.position[0], y=self.position[1])
            self.dictionary.show()
        except:
            print(traceback.format_exc())
            
    # Chương trình xử lý sự kiện Start để chạy hiển thị thư viện.
    def StartDictionary(self, event=None):
        try:
            if(self.is_alive()):
                if self.runing:
                    pass
                else:
                    self.continues()
                pass
            else:
                self.start()
            pass
        except:
            print(traceback.format_exc())
        # print("test")
        pass
    
    # Chương trình xử lý không hiển thị thư viện khi chạy.
    def StopDictionary(self):
        if self.runing:
            self.dictionary.hide()
            self.stop()

    def LoadKey(self, data=None):
        try:
            if data == None:
                data = self.FP.GetJson(self.FP.jsonPath)
            else:
                pass
            self.key = data['Key']
        except:
            print(traceback.format_exc())

    # Hàm load thông tin tìm kiểm trên dictionary thông qua URL. Nếu URL sẽ được lấy trong data nếu data được nhập vào.
    def LoadUrl(self, data=None, url=None, value="$value$"):
        if data != None:
            try:
                self.url = data['Dictonary'][data['Select']]['website']
                self.value = data['Dictonary'][data['Select']]['value']
            except:
                print(traceback.format_exc())
        elif url != None:
            self.url = url
            self.value = value
        else:
            return False
        try:
            url = self.url.split(self.value)[0] + self.copy_clipboard() + self.url.split(self.value)[1]
            self.dictionary.LoadUrl(url)
        except:
            try:
                url = self.url.split(self.value)[0] + self.copy_clipboard()
                self.dictionary.LoadUrl(url)
            except:
                print(traceback.format_exc())

    # Hàm kiểm tra trạng thái nhận vào từ chuột.
    def Check_Mouse(self):
        try:
            if self.mouse_key_input.mouse_left_click:
                self.check_mouse = True # Báo chuột vừa được nhấn.
                self.position = self.mouse_key_input.Get_Position()
                self.time = time.time() # Dùng để check timeout kiểm tra thời gian chuột vừa nhấn xong.
                pass
            else:
                if self.check_mouse and time.time() - self.time > 1:
                    self.check_mouse = False
                pass
            # Kiểm tra trạng thái màn hình:
            if self.dictionary.hide_state == False and self.check_mouse:
                # Kiểm tra vị trí click chuột có nằm trong vị chí của màn hình hay không, nếu không ẩn màn hình.
                if (self.position[0] < self.dictionary.position[0] or self.position[0] > self.dictionary.position[0] + self.dictionary.master.winfo_width() or 
                    self.position[1] < self.dictionary.position[1] or self.position[1] > self.dictionary.position[1] + self.dictionary.master.winfo_height()):
                    if self.print:
                        print("Mouse Hide:", str(self.dictionary.hide_state), "check mouse:", str(self.check_mouse))
                    self.dictionary.hide()
                pass
        except:
            print(traceback.format_exc())

    # Hàm kiểm tra trạng thái nhập vào từ bàn phím.
    def Check_Key(self):
        try:
            if len(self.mouse_key_input.list_key) == 1 and self.mouse_key_input.key_pressed != "None":
                if self.mouse_key_input.list_key[0] == self.key and self.check_key == False and self.start_record_key == False and self.check_mouse:
                    self.check_key = True
                    if self.print:
                        print("check Key")
                self.time_key = time.time()

            elif len(self.mouse_key_input.list_key) == 0:
                if self.runing and self.check_key and self.start_record_key == False:
                    self.position = self.mouse_key_input.Get_Position()
                    self.Dictionary_Run()
                    print("check Key: run")
                self.check_key = False
                self.start_record_key = False
                self.time_key = time.time()

            else:
                if time.time() - self.time_key > 1:
                    self.mouse_key_input.Remove_List_Key()
                self.start_record_key = True
                self.check_key = False
                if self.print:
                    print("check Key: stop, len:", str(len(self.mouse_key_input.list_key)))
        except:
            print(traceback.format_exc())

    # Hàm trả về dữ liệu copy từ clipboard:
    def copy_clipboard(self):
        try:
            pyperclip.copy("") # <- This prevents last copy replacing current copy of null.
            pya.hotkey('ctrl', 'c')
            time.sleep(0.3)  # ctrl-c is usually very fast but your program may execute faster 
            return str(pyperclip.paste())
        except:
            print(traceback.format_exc())
            return ""

    # Hàm thực thi kiểm tra Dictionary:
    def run_after(self):
        if self.runing:
            self.LoadKey()
            self.Check_Mouse()
            self.Check_Key() 
            self.mouse_key_input.print = self.print
        else:
            pass
        self.dictionary.master.after(20, self.run_after)

    # Hàm chạy Thread để kiểm tra trạng thái hiển thị của Dictionary.
    def run(self):
        self.runing = True
        self.mouse_key_input.start()
        self.dictionary.Init()
        while True:
            self.dictionary.master.after(10, self.run_after)
            self.dictionary.master.mainloop()
    
    # Chuyển trạng thái dừng hoạt động hiển thị của Dictionaray.
    def stop(self):
        self.runing = False
        if self.print:
            print("Pause")
    
    # Chuyển trạng thái tiếp tục hoạt động của Dictionary.
    def continues(self):
        self.runing = True
        if self.print:
            print("Continue")
