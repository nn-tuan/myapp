from ctypes.wintypes import MSG
from doctest import master
import traceback
try:
    import os

    import tkinter as TK
    from tkinter import ttk
    from tkinter import messagebox as MSG
except:
    print(traceback.format_exc())

class MainView():
    def __init__(self, stateVM, path):
        self.path = path
        self.stateVM = stateVM
        self.layout_init()
        self.center(self.root)
        self.creat_item()
        self.hide_flag = False
        pass

    # Khởi tạo màn hình
    def layout_init(self, geometry='550x150', title="Dictionary App", bg="white", minsize=[550, 150], maxsize=[1366, 768]):
        self.root = TK.Tk()
        self.root.eval('tk::PlaceWindow . center')
        self.root.iconbitmap(self.path + "\Data\Icon\Dictionary_1.ico")
        self.root.resizable(False, False)
        self.root.geometry(geometry)
        self.root.title(title)
        self.root.configure(background=bg)
        self.root.minsize(minsize[0], minsize[1])
        self.root.maxsize(maxsize[0], maxsize[1])
        self.msg = MSG
    
    # Chỉnh màn hình app vào chính giữa màn hình PC
    def center(self, win):
        win.update_idletasks()

        width = win.winfo_width()
        frm_width = win.winfo_rootx() - win.winfo_x()
        win_width = width + 2 * frm_width

        height = win.winfo_height()
        titlebar_height = win.winfo_rooty() - win.winfo_y()
        win_height = height + titlebar_height + frm_width

        x = win.winfo_screenwidth() // 2 - win_width // 2
        y = win.winfo_screenheight() // 2 - win_height // 2
        win.geometry('{}x{}+{}+{}'.format(width, height, x, y))
        win.deiconify()

    # Khởi tạo item trên màn hình chính
    def creat_item(self):
        self.auto_select_label = TK.Label(bg="#4169e1", fg="white", font=('Arial', 9), anchor="nw", justify=TK.LEFT)
        self.auto_select_label.place(x=15, y= 5, width=520, height=135)

        self.auto_select_label = TK.Label(text="Select\nDictionary", bg="#4169e1", fg="white", font=('Arial', 9), anchor="nw", justify=TK.LEFT)
        self.auto_select_label.place(x=20, y= 30, width=80, height=40)

        self.select_item = TK.StringVar()
        self.list_item = ttk.Combobox(self.root, textvariable=self.select_item)
        self.list_item['values'] = ["Test", "OK", "Python"]
        self.list_item['state'] = 'readonly'
        self.list_item.place(x=90, y=30, width=435, height=40)
        
        run_label_x = 90
        self.start_img = TK.PhotoImage(file=self.path + "\Data\Icon\Image\\test.png").subsample(15, 15)
        self.start_label = TK.Button(text="Start", bg="#5b9bd5", fg="white", font=('Arial', 11))
        self.start_label.place(x=run_label_x, y= 90, width=100, height=40)

        self.setting_img = TK.PhotoImage(file=self.path + "\Data\Icon\Image\setting.png").subsample(15, 15)
        self.setting_label = TK.Button(text="Setting", bg="#5b9bd5", fg="white", font=('Arial', 11))
        self.setting_label.place(x=225, y= 90, width=100, height=40)

        self.hide_img = TK.PhotoImage(file=self.path + "\Data\Icon\Image\hide.png").subsample(15, 15)
        self.hide_label = TK.Button(text="Hide", bg="#5b9bd5", fg="white", font=('Arial', 11))
        self.hide_label.place(x= 450 - run_label_x, y= 90, width=100, height=40)

        
        self.start_label.config(image=self.start_img, compound=TK.LEFT, justify="left")
        self.setting_label.config(image=self.setting_img, compound=TK.LEFT, justify="left")
        self.hide_label.config(image=self.hide_img, compound=TK.LEFT, justify="left")

    # Hàm binding, giúp bind Fnc vào nút nhất, item...
    def Binding(self, item, sequence=None, func=None, add=None):
        try:
            return item.bind(sequence, func)
        except:
            return None
    
    # Hàm unbinding, giúp bind Fnc vào nút nhất, item...
    def UnBinding(self, item, sequence=None, funcid=None):
        try:
            item.unbind(sequence, funcid)
            return "OK"
        except:
            return traceback.format_exc()

    # Hàm loop: MainWindows sẽ chạy liên tục nếu hàm này được gọi.
    def loop(self):
        self.root.mainloop()
        pass