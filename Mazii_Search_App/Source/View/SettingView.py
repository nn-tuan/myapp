from tkinter import DISABLED, FLAT, RAISED, SUNKEN, Entry
import traceback
from turtle import bgcolor
try:
    import os

    import tkinter as TK
    from tkinter import ttk
    from tkinter import messagebox as MSB
except:
    print(traceback.format_exc())


class SettingView(TK.Toplevel):
    def __init__(self, root, path):
        TK.Toplevel.__init__(self, master= root)
        self.path = path
        self.layout_init()
        self.center(self)
        self.creat_item()

    # Chỉnh màn hình App vào chính giữa màn hình PC:
    def center(self, win):
        win.update_idletasks()
        width = win.winfo_width()
        frm_width = win.winfo_rootx() - win.winfo_x()
        win_width = width + 2 * frm_width
        height = win.winfo_height()
        titlebar_height = win.winfo_rooty() - win.winfo_y()
        win_height = height + titlebar_height + frm_width
        x = win.winfo_screenwidth() // 2 - win_width // 2
        y = win.winfo_screenheight() // 2 - win_height // 2
        win.geometry('{}x{}+{}+{}'.format(width, height, x, y))
        win.deiconify()

    # Tạo ra layout cơ bản, kích thước màn hình, các thông số mặc định của màn hình:
    def layout_init(self, title="Setting:"):
        self.master.eval(f'tk::PlaceWindow {str(self)} center')
        self.iconbitmap(self.path + "\Data\Icon\Dictionary_1.ico")
        self.title(title)
        self.geometry("560x400")
        self.resizable(False, False)
        self.msg = MSB
        # TK.Label(self, text ="This is a new window").pack()   
        pass

    # Tạo ra các label, item, button trên màn hình:
    def creat_item(self):
        #==========================### Màu nền ###==========================#
        self.auto_select_label = TK.Label(self, bg="#4169e1", fg="white", font=('Arial', 9), anchor="nw", justify=TK.LEFT)
        self.auto_select_label.place(x=15, y= 5, width=530, height=385)

        #=====### Khu vực ComboBox hiển thị các Dictionary đã lưu ###=====#
        self.auto_select_label = TK.Label(self, text="Select\nDictionary", bg="#4169e1", fg="white", font=('Arial', 9), anchor="nw", justify=TK.LEFT)
        self.auto_select_label.place(x=20, y= 30, width=80, height=40)

        self.select_item = TK.StringVar(self)
        self.list_item = ttk.Combobox(self, textvariable=self.select_item)
        self.list_item['values'] = ["Test", "OK", "Python"]
        self.list_item['state'] = 'readonly'
        self.list_item.place(x=90, y=30, width=420, height=40)

        #============### Khu vực sửa thông tin Dictionary ###============#
        self.label = TK.Label(self, text="Dictionary\nName:", bg="#4169e1", fg="white", font=('Arial', 9), anchor="nw", justify=TK.LEFT)
        self.label.place(x=20, y= 100, width=80, height=40)
        self.Name_text = TK.Text(master=self, bg="#f0f8ff", fg="black", font=('Arial', 11))
        self.Name_text.place(x=90, y=100, width=275, height=30)

        self.label = TK.Label(self, text="Value:", bg="#4169e1", fg="white", font=('Arial', 9), anchor="nw", justify=TK.LEFT)
        self.label.place(x=365, y= 100, width=50, height=40)
        self.Value_text = TK.Text(master=self, bg="#f0f8ff", fg="black", font=('Arial', 11))
        self.Value_text.place(x=410, y=100, width=100, height=30)

        self.label = TK.Label(self, text="Dictionary\nURL:", bg="#4169e1", fg="white", font=('Arial', 9), anchor="nw", justify=TK.LEFT)
        self.label.place(x=20, y= 150, width=80, height=40)
        self.URL_text = TK.Text(master=self, bg="#f0f8ff", fg="black", font=('Arial', 11))
        self.URL_text.place(x=90, y=150, width=420, height=30)

        run_label_x = 90
        self.add_img = TK.PhotoImage(file=self.path + "\Data\Icon\Image\\add.png").subsample(15, 15)
        self.add_label = TK.Button(master=self, text="Add", bg="#5b9bd5", fg="white", font=('Arial', 11), activebackground="#5b9bd5", activeforeground="white")
        self.add_label.place(x=run_label_x, y= 200, width=100, height=40)
        
        self.edit_img = TK.PhotoImage(file=self.path + "\Data\Icon\Image\edit.png").subsample(15, 15)
        self.edit_label = TK.Button(master=self, text="Edit", bg="#5b9bd5", fg="white", font=('Arial', 11), activebackground="#5b9bd5", activeforeground="white")
        self.edit_label.place(x=250, y= 200, width=100, height=40)

        self.delete_img = TK.PhotoImage(file=self.path + "\Data\Icon\Image\delete.png").subsample(4, 4)
        self.delete_label = TK.Button(master=self, text="Delete", bg="#696969", fg="white", font=('Arial', 11), activebackground="#5b9bd5", activeforeground="white")
        self.delete_label.place(x= 500 - run_label_x, y= 200, width=100, height=40)
        
        self.add_label.config(image=self.add_img, compound=TK.LEFT, justify="left")
        self.edit_label.config(image=self.edit_img, compound=TK.LEFT, justify="left")
        self.delete_label.config(image=self.delete_img, compound=TK.LEFT, justify="left")
        
        #================### CheckBox chọn phím tắt ###================#
        self.label = TK.Label(self, text="Function shortcut setting:", bg="#4169e1", fg="white", font=('Arial', 9), anchor="nw", justify=TK.LEFT)
        self.label.place(x=20, y= 260)

        self.check_var_1 = TK.IntVar()
        self.check_var_2 = TK.IntVar()
        self.check_var_3 = TK.IntVar()

        self.check_item_1 = TK.Checkbutton(master=self, text='Mouse + Ctrl',variable=self.check_var_1, onvalue=1, offvalue=0, background="#4169e1", fg="white", selectcolor="#778899", activebackground="#4169e1", activeforeground="white")
        self.check_item_1.place(x=60, y=280)
        self.check_item_2 = TK.Checkbutton(master=self, text='Mouse + Alt',variable=self.check_var_2, onvalue=1, offvalue=0, background="#4169e1", fg="white", selectcolor="#778899", activebackground="#4169e1", activeforeground="white")
        self.check_item_2.place(x=200, y=280)
        self.check_item_3 = TK.Checkbutton(master=self, text='Mouse + Key:',variable=self.check_var_3, onvalue=1, offvalue=0, background="#4169e1", fg="white", selectcolor="#778899", activebackground="#4169e1", activeforeground="white")
        self.check_item_3.place(x=340, y=280)
        
        self.Key_text = TK.Text(master=self, bg="#f0f8ff", fg="black", font=('Arial', 11))
        self.Key_text.place(x=440, y=280, width=80, height=25)
        self.Key_text.config(state=DISABLED)
        self.Key_text.config(autoseparators=False)

        #===================### Nút SAVE và QUIT ###===================#
        run_label_x = 90
        self.save_img = TK.PhotoImage(file=self.path + "\Data\Icon\Image\\save.png").subsample(17, 17)
        self.save_label = TK.Button(master=self, text="Save", bg="#5b9bd5", fg="white", font=('Arial', 11))
        self.save_label.place(x=run_label_x, y= 335, width=100, height=40)

        self.quit_img = TK.PhotoImage(file=self.path + "\Data\Icon\Image\quit.png").subsample(10, 10)
        self.quit_label = TK.Button(master=self, text="Quit", bg="#696969", fg="white", font=('Arial', 11))
        self.quit_label.place(x= 500 - run_label_x, y= 335, width=100, height=40)

        self.save_label.config(image=self.save_img, compound=TK.LEFT, justify="left")
        self.quit_label.config(image=self.quit_img, compound=TK.LEFT, justify="left")
        pass

    # Hàm thay đổi nội dung trên ô TextBox
    def TextInsert(self, item, text):
        try:
            item.delete(0.0, TK.END)
            item.insert(TK.END, text)
        except:
            print(traceback.format_exc())
    
    def GetText(self, item):
        try:
            return item.get(0.0, TK.END)
        except:
            return None
    
    # Hàm binding, giúp bind Fnc vào nút nhất, item...
    def Binding(self, item, sequence=None, func=None, add=None):
        try:
            return item.bind(sequence, func)
        except:
            try:
                item.config(command=func)
                return None
            except:
                return None

    # Hàm Command, giúp comand Fnc vào checkbox item...
    def Command(self, item, func=None, add=None):
        try:
            item.config(command=func)
            return None
        except:
            return None
    
    # Hàm unbinding, giúp bind Fnc vào nút nhất, item...
    def UnBinding(self, item, sequence=None, funcid=None):
        try:
            item.unbind(sequence, funcid)
            return "OK"
        except:
            return traceback.format_exc()

    # Hàm loop: Windows sẽ chạy liên tục nếu hàm này được gọi.
    def loop(self):
        self.mainloop()
        pass