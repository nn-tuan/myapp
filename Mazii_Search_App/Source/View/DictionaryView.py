import traceback
try:
    import tkinter as TK
    from View.Browser import *
except:
    print(traceback.format_exc())
    pass
#===========================================================================================#
# Class khởi tạo màn hình Dictionary lúc Search:
#===========================================================================================#
class Dictionary_Search():
    def __init__(self) -> None:
        pass

    def Init(self):
        assert cef.__version__ >= "55.3", "CEF Python v55.3+ required to run this"
        sys.excepthook = cef.ExceptHook 
        self.master = TK.Tk()

        self.is_first_run = True
        self.url = "https://www.google.com"
        self.position = (0, 0)
        self.hide_state = False

        self.creat_window()
        pass

    def update(self, event):
        self.position = (self.master.winfo_x(), self.master.winfo_y())

    # Hàm khởi tạo màn hình Search Dictionary
    def creat_window(self):
        self.check = True
        self.master.geometry("640x720")
        self.master.title("Dictionary")
        self.master.resizable(False, False)
        self.master.bind('<Configure>', self.update)
        self.center()
        self.hide()
        
        self.run()
        self.master.protocol("WM_DELETE_WINDOW", self.hide)
    
    # Di chuyển màn hình đến vị trí X, Y
    def move_window(self, width=0, height=0, x=0, y=0):
        if width == 0:
            width = self.master.winfo_width()
        if height == 0:
            height = self.master.winfo_height()
        
        frm_width = self.master.winfo_rootx() - self.master.winfo_x()
        win_width = width + 2 * frm_width

        titlebar_height = self.master.winfo_rooty() - self.master.winfo_y()
        win_height = height + titlebar_height + frm_width

        x_check = self.master.winfo_screenwidth() - x - win_width
        y_check = self.master.winfo_screenheight() - y - win_height
        # if x_check <= 0:
        #     x += x_check
        if y_check <= 0:
            y += y_check

        self.master.geometry('{}x{}+{}+{}'.format(width, height, x, y))
        self.master.deiconify()
        self.position = (x, y)
        pass

    # Chỉnh màn hình app vào chính giữa màn hình PC
    def center(self):
        self.master.update_idletasks()

        width = self.master.winfo_width()
        frm_width = self.master.winfo_rootx() - self.master.winfo_x()
        win_width = width + 2 * frm_width

        height = self.master.winfo_height()
        titlebar_height = self.master.winfo_rooty() - self.master.winfo_y()
        win_height = height + titlebar_height + frm_width

        x = self.master.winfo_screenwidth() // 2 - win_width // 2
        y = self.master.winfo_screenheight() // 2 - win_height // 2
        self.master.geometry('{}x{}+{}+{}'.format(width, height, x, y))
        self.master.deiconify()
        self.position = (x, y)

    # Hàm set giá trị URL
    def set(self, url):
        self.url = url
        pass

    # Hàm chạy URL nhập mới vào:
    def LoadUrl(self, url):
        try:
            if self.is_first_run:
                self.app = MainFrame(self.master, url)
                pass
            else:
                self.app.browser_frame.browser.LoadUrl(url)
        except:
            print(traceback.format_exc())

    # Hiển thị window
    def show(self):
        if self.is_first_run:
            self.is_first_run = False
        try:
            self.hide_state = False
            self.master.deiconify()
            print("Show")
        except:
            print(traceback.format_exc())
    
    # Ẩn Window
    def hide(self):
        """
        Ẩn màn hình đang hoạt động.
        """
        try:
            self.hide_state = True
            self.master.withdraw()
            print("Hide")
        except:
            print(traceback.format_exc())

    # Hàm gọi màn hình Search Dictionary khởi động
    def run(self):
        try:
            self.settings = {}
            cef.Initialize(settings=self.settings)
        except:
            print(traceback.format_exc())

    # Hàm tắt màn hình Search Dictionary
    def stop(self):
        try:
            # self.app.on_close()
            self.app.close = True
        except:
            pass

