import traceback

try:
    import sys, os
    import time

    from tkinter import *
    from threading import Thread

    import pyautogui as pya
    import pyperclip
    from cefpython3 import cefpython as cef
    import ctypes
    import mouse
    import keyboard
    import Model.FileProcess as File
except:
    print(traceback.format_exc())
    input("Error!")



IMAGE_EXT = ".png" if TkVersion > 8.5 else ".gif"

web_run = False

mouse_x = 10
mouse_y = 10

positon = mouse.get_position()

class Web():
    def __init__(self, x, y, url):
        global mouse_x, mouse_y
        self.url = url
        mouse_x = x
        mouse_y = y
        assert cef.__version__ >= "55.3", "CEF Python v55.3+ required to run this"
        sys.excepthook = cef.ExceptHook 
        self.check = False
        pass

    def run(self):
        global mouse_x, mouse_y, web_run
        web_run = True
        http_url = self.url
        if mouse_y >= 250:
            mouse_y = 250
        if self.check == False:
            self.root = Tk()
            self.check = True
            self.root.resizable(False, False)
            self.settings = {}
            self.app = MainFrame(self.root, http_url)
            cef.Initialize(settings=self.settings)
            self.app.mainloop()
            self.check = False
        else:
            pass

    def stop(self):
        try:
            self.app.on_close()
        except:
            pass
    
class MainFrame(Frame):

    def __init__(self, root, http_url):
        global position

        self.first_run = False

        self.browser_frame = None
        self.navigation_bar = None
        self.root = root
        self.url = http_url

        # Root
        root.geometry("640x720+" + str(mouse_x) + "+" + str(mouse_y))

        # MainFrame
        Frame.__init__(self, root)
        self.master.title("Dictionary")
        self.master.protocol("WM_DELETE_WINDOW", self.on_close)


        # BrowserFrame
        self.browser_frame = BrowserFrame(self, self.navigation_bar, self.url)
        self.browser_frame.grid(row=1, column=0,
                                sticky=(N + S + E + W))
        Grid.rowconfigure(self, 1, weight=1)
        Grid.columnconfigure(self, 0, weight=1)

        # Pack MainFrame
        self.pack(fill=BOTH, expand=YES)
        self.check_move()

    def check_move(self):
        global mouse_x, mouse_y, positon

        mouse_x = self.winfo_rootx()
        mouse_y = self.winfo_rooty()
        self.after(1, self.check_move)

    def on_close(self):
        if self.browser_frame:
            self.browser_frame.on_root_close()
            self.browser_frame = None
        else:
            self.master.destroy()

class BrowserFrame(Frame):
    def __init__(self, mainframe, navigation_bar=None, http_url="google.com"):
        self.navigation_bar = navigation_bar
        self.closing = False
        self.browser = None
        self.url = http_url
        Frame.__init__(self, mainframe)
        self.mainframe = mainframe
        self.bind("<FocusIn>", self.on_focus_in)
        self.bind("<Configure>", self.on_configure)
        """For focus problems see Issue #255 and Issue #535. """
        self.focus_set()

    def embed_browser(self):
        window_info = cef.WindowInfo()
        rect = [0, 0, self.winfo_width(), self.winfo_height()]
        window_info.SetAsChild(self.get_window_handle(), rect)
        
        # print(self.url)
        self.browser = cef.CreateBrowserSync(window_info, url=self.url)
        assert self.browser
        self.browser.SetClientHandler(LifespanHandler(self))
        self.browser.SetClientHandler(LoadHandler(self))
        self.browser.SetClientHandler(FocusHandler(self))
        self.message_loop_work()

    def get_window_handle(self):
        if self.winfo_id() > 0:
            return self.winfo_id()
        else:
            raise Exception("Couldn't obtain window handle")

    def message_loop_work(self):
        cef.MessageLoopWork()
        self.after(20, self.message_loop_work)

    def on_configure(self, _):
        if not self.browser:
            self.embed_browser()

    def on_root_configure(self):
        # Root <Configure> event will be called when top window is moved
        if self.browser:
            self.browser.NotifyMoveOrResizeStarted()

    def on_mainframe_configure(self, width, height):
        if self.browser:
            ctypes.windll.user32.SetWindowPos(
                self.browser.GetWindowHandle(), 0,
                0, 0, width, height, 0x0002)
            self.browser.NotifyMoveOrResizeStarted()

    def on_focus_in(self, _):
        # root_log.debug("BrowserFrame.on_focus_in")
        if self.browser:
            self.browser.SetFocus(True)


    def on_root_close(self):
        # root_log.info("BrowserFrame.on_root_close")
        if self.browser:
            # root_log.debug("CloseBrowser")
            self.browser.CloseBrowser(True)
            self.clear_browser_references()
        else:
            # root_log.debug("Frame.destroy")
            self.destroy()
            

    def clear_browser_references(self):
        # Clear browser references that you keep anywhere in your
        # code. All references must be cleared for CEF to shutdown cleanly.
        self.browser = None

class LifespanHandler(object):

    def __init__(self, tkFrame):
        self.tkFrame = tkFrame

    def OnBeforeClose(self, browser, **_):
        # root_log.debug("LifespanHandler.OnBeforeClose")
        self.tkFrame.quit()

class LoadHandler(object):

    def __init__(self, browser_frame):
        self.browser_frame = browser_frame

    def OnLoadStart(self, browser, **_):
        if self.browser_frame.master.navigation_bar:
            self.browser_frame.master.navigation_bar.set_url(browser.GetUrl())

class FocusHandler(object):
    """For focus problems see Issue #255 and Issue #535. """

    def __init__(self, browser_frame):
        self.browser_frame = browser_frame

    def OnTakeFocus(self, next_component, **_):
        # root_log.debug("FocusHandler.OnTakeFocus, next={next}"
                    #  .format(next=next_component))
        pass

    def OnSetFocus(self, source, **_):
        return True

    def OnGotFocus(self, **_):
        # root_log.debug("FocusHandler.OnGotFocus")
        pass

class Tabs(Frame):

    def __init__(self):
        Frame.__init__(self)
        # TODO: implement tabs


time_click = 0
def On_Left_Click():
    global printflag, timeout, web, web_run, mouse_x, mouse_y, position, time_click
    if time.time() - time_click > 0.5:
        time_click = time.time()
    else:
        return 0
    
    if web_run:
        positon = mouse.get_position()
        if positon[0] < mouse_x or positon[0] > mouse_x + 640 or positon[1] < mouse_y - 50 or positon[1] > mouse_y + 720:
            web.stop()
            pass
        pass
    else:
        printflag = True
        timeout = time.time()

def On_Right_Click():
    global web_run, web
    if web_run:
        p = mouse.get_position()
        # print(p)
        if p[0] < mouse_x or p[0] > mouse_x + 640 or p[1] < mouse_y or p[1] > mouse_y + 720:
            web.stop()
            pass

middle_is_press = False    
def On_Middle_Click():
    global middle_is_press, timeout
    if middle_is_press:
        middle_is_press = False
    else:
        middle_is_press = True
    timeout = time.time()
    # print(middle_is_press)


def copy_clipboard():
    try:
        pyperclip.copy("") # <- This prevents last copy replacing current copy of null.
        pya.hotkey('ctrl', 'c')
        time.sleep(0.3)  # ctrl-c is usually very fast but your program may execute faster 
        return pyperclip.paste()
    except:
        print(traceback.format_exc())
        return ""
        #input("Error!")

class Dictionary_Run(Thread):
    def __init__(self, File=File):
        Thread.__init__(self)
        self.runing = False
        self.FP = File
        pass

    # Hàm set giá trị URL của thư viện và data cần thiết để thư viện tìm kiếm.
    def SetUrl(self, data, value):
        self.url = data
        self.value = value

    # Hàm chạy Thread để kiểm tra trạng thái hiển thị của Dictionary.
    def run(self):
        global mouse_x, mouse_y, web, web_run, timeout, printflag, middle_is_press
        self.runing = True
        mouse.on_click(On_Left_Click)
        mouse.on_right_click(On_Right_Click)
        mouse.on_middle_click(On_Middle_Click)

        web = Web(0, 0, "")
        timeout = 0
        printflag = False
        while True:
            if self.runing:
                if printflag:
                    # if keyboard.is_pressed("ctrl") and web_run == False:
                    if (web_run == False and middle_is_press)  or (keyboard.is_pressed("ctrl") and web_run == False):
                        middle_is_press = False
                        web_run = True
                        p = mouse.get_position()
                        printflag = False
                        try:
                            url = self.url.split(self.value)[0] + str(copy_clipboard()) + self.url.split(self.value)[1]
                        except:
                            url = self.url.split(self.value)[0] + str(copy_clipboard())

                        # web = Web(p[0], p[1], r)
                        web.url = url
                        mouse_x = p[0]
                        mouse_y = p[1]
                        web.run()
                        time.sleep(0.01)
                        web_run = False
                    else:
                        printflag = False
                        time.sleep(0.1)
                else:
                    if middle_is_press:
                        if time.time() - timeout > 2:
                            middle_is_press = False
                    time.sleep(0.1)
            else:
                time.sleep(1)
    
    # Chuyển trạng thái dừng hoạt động hiển thị của Dictionaray.
    def stop(self):
        self.runing = False
        print("Pause")
    
    # Chuyển trạng thái tiếp tục hoạt động của Dictionary.
    def continues(self):
        self.runing = True
        print("Continue")

     # Hàm chuyển trạng thái của Dictionary thành Run, truyền thông số của thư viện đã lưu cho Dictionary.
    def Dictionary_Run(self):
        if self.runing != True:
            with open(self.FP.jsonPath, "r+") as json_file:
                data = self.FP.load(json_file)
                json_file.close()
            self.SetUrl( data['Dictonary'][data['Select']]['website'], 
                                    data['Dictonary'][data['Select']]['value']
                                    )
            self.continues()

    # Chương trình xử lý sự kiện Start để chạy hiển thị thư viện.
    def StartDictionary(self, event=None):
        try:
            if(self.is_alive()):
                if self.runing:
                    pass
                else:
                    self.Dictionary_Run()
                pass
            else:
                self.start()
                self.Dictionary_Run()
            pass
        except:
            print(traceback.format_exc())
        # print("test")
        pass
    
    # Chương trình xử lý không hiển thị thư viện khi chạy.
    def StopDictionary(self):
        if self.runing:
            self.stop()