import traceback
try:
    from pynput.keyboard import Key, Listener
    import mouse
    from threading import Thread
except:
    print(traceback.format_exc())
    input("Input Modle is Error!\nPlease press any key to exit.")

class Input(Thread):
    def __init__(self) -> None:
        Thread.__init__(self)
        self.mouse_left_click   = False
        self.mouse_right_click  = False
        self.mouse_middle_click = False
        self.key_pressed        = "None"
        self.list_key           = []

        self.print = False
        self.all_event_start = "ALL"

    def set_check_position(self,    left_down_position=False, left_up_position=False, 
                                    right_down_position=False, right_up_position=False, 
                                    middle_down_position=False, middle_up_position=False):
        self.left_down_position     = left_down_position
        self.left_up_position       = left_up_position
        self.right_down_position    = right_down_position
        self.right_up_position      = right_up_position
        self.middle_down_position   = middle_down_position
        self.middle_up_position     = middle_up_position
        self.position = mouse.get_position()
        pass
    
    # Hàm remove all item trong lish key:
    def Remove_List_Key(self):
        if self.key_pressed == "None":
            if len(self.list_key) > 0:
                self.list_key = []
                print("List Key removed!")
            else:
                pass
        else:
            pass

    # Hàm bắt sự kiện chuột trái nhấn.
    def On_Left_Down(self):
        try:
            if self.left_down_position:
                self.position = mouse.get_position()
        except:
            pass
        self.mouse_left_click = True
        pass

    # Hàm bắt sự kiện chuột trái nhả.
    def On_Left_Up(self):
        try:
            if self.left_up_position:
                self.position = mouse.get_position()
        except:
            pass
        self.mouse_left_click = False
        pass

    # Hàm bắt sự kiện chuột phải được nhấn.
    def On_Right_Down(self):
        try:
            if self.right_down_position:
                self.position = mouse.get_position()
        except:
            pass
        self.mouse_right_click = True
        pass

    # Hàm bắt sự kiện chuột phải nhả.
    def On_Right_Up(self):
        try:
            if self.right_up_position:
                self.position = mouse.get_position()
        except:
            pass
        self.mouse_right_click = False
        pass

    # Hàm bắt sự kiện chuột giữa được nhấn.
    def On_Middle_Down(self):
        try:
            if self.middle_down_position:
                self.position = mouse.get_position()
        except:
            pass
        self.mouse_middle_click = True
        pass

    # Hàm bắt sự kiện chuột giữa nhả.
    def On_Middle_Up(self):
        try:
            if self.middle_up_position:
                self.position = mouse.get_position()
        except:
            pass
        self.mouse_middle_click = False
        pass

    # Hàm khai báo sự kiện, định nghĩa các hàm Callback.
    def Mouse_Evnt(self):
        mouse.on_button(self.On_Left_Down, (), [mouse.LEFT], [mouse.DOWN])
        mouse.on_button(self.On_Right_Down, (), [mouse.RIGHT], [mouse.DOWN])
        mouse.on_button(self.On_Middle_Down, (), [mouse.MIDDLE], [mouse.DOWN])

        mouse.on_click(self.On_Left_Up)
        mouse.on_right_click(self.On_Right_Up)
        mouse.on_middle_click(self.On_Middle_Up)
        pass

    # Hàm bắt sự kiện bàn phím được nhấn.
    def On_Key_Press(self, key):
        self.key_pressed = "{0}".format(key)
        if "{0}".format(key) not in self.list_key:
            self.list_key.append("{0}".format(key))
            if self.print:
                print('{0} pressed'.format(key))
        else:
            pass

    # Hàm bắt sự kiện bàn phím nhả.zzzzzzzzzzz
    def On_Key_Release(self, key):
        self.key_pressed = "None"
        if "{0}".format(key) in self.list_key:
            self.list_key.remove("{0}".format(key))
            if self.print:
                print('{0} release'.format(key))
        else:
            pass
        
    # Hàm lấy tọa đọ của chuột
    def Get_Position(self):
        """
        Hàm lấy tọa độ của chuột hiện tại trên màn hình. 
        Trả vè (x, y): Tọa độ x, y của chuột.
        """
        return mouse.get_position()

    # Hàm dừng chế độ bắt sự kiện input
    def Key_Stop_Evnt(self):
        try:
            self.key_pressed = "None"
            self.list_key = []
            self.listener.stop()
        except:
            pass
    
    # Hàm bắt sự kiện nhận vào từ bàn phím
    def Key_Evnt(self):
        # Collect events until released
        with Listener(
                        on_press=self.On_Key_Press,
                        on_release=self.On_Key_Release
                     ) as self.listener : self.listener.join()
        pass

    # Hàm khởi động chương trình.
    def run(self):
        if self.all_event_start == "ALL":
            self.Mouse_Evnt()
            self.Key_Evnt()
        elif self.all_event_start == "KEY":
            self.Key_Evnt()
        else:
            self.Mouse_Evnt()
