import os
import traceback
from json import *

# Đường dẫn mặc định. Folder Output mặc định và file .env mặc định sẽ dẫn theo đường dẫn động.
path = os.getcwd()
filePath =  path + "/Output/"
envPath  =  path + "/.env"
jsonPath = path + "/cfg.txt"

# Hàm set lại địa chỉ Path, mặc định là địa chỉ của module này.
def set_os_path(os_path="."):
    """
    Hàm set lại thông tin xử lý đối với file:
    + os_path: set lại địa chỉ Path, mặc định là địa chỉ của module này.
    """
    global path, filePath, envPath, jsonPath
    path = os_path

    filePath =  path + "/Output/"
    envPath  =  path + "/.env"
    jsonPath = path + "/cfg.txt"

    return 1

# Hàm set giá trị file output và file env. Nếu không set sẽ lấy giá trị mặc định.
def set(env_path=".env", out_path=".Output"):
    """
    Hàm set lại thông tin xử lý đối với file:
    + filePath: Đường dẫn tới thư mục Output khi sử dụng file output. (Logfile)
    + envPath: Đường dẫn tới file .env dùng để setup, lưu trữ thông tin cần thiết trong quá trình chạy chương trình. 
    """
    global envPath, filePath
    try:
        envPath = env_path
        filePath = out_path
        return True
    except:
        return False
    pass

# Hàm lấy đường dẫn file env.
def get_envpath():
    return envPath

# Hàm lấy đường dẫn file output.
def get_filepath():
    return filePath

# Hàm tìm chuỗi str trong chuỗi data. Nếu có trả về True, ngược lại trả về False.
def Find(data, str):
    """
    Hàm giúp tìm chuỗi str trong chuỗi data. 
    + Nếu tìm thấy trả về True.
    + Nếu không tìm thấy trả về False.
    """
    if (data.find(str, 0, len(str)) > -1):
        return 1
    else:
        return 0

# Hàm sửa lại giá trị trong dòng old_line cũ trong file. Nếu không tồn tại old_line sẽ thêm mới vào file.
def file_print(new_line, old_line="None"):
    """
    Hàm in ra file txt, vẫn lưu lại nội dung cũ, không tạo mới file nếu file đã tồn tại.
    + old_line: Dòng cũ đã tồn tại trong file đã ghi. Nếu không có hoặc không quan tâm để trốn (Null or "").
    + new_line: Dòng mới sẽ thay thế old_line. Nếu không tồn tại old_line sẽ ghi mới vào file.
    """
    global filePath
    
    print(new_line)
    retext = False
    try:
        try:
            file = open(filePath + "output.txt", 'r+', encoding= 'utf-8')
            pass
        except:
            if input(traceback.format_exc() + "\nDo you want to creat a new file in directory:" + filePath + "\nY/N:").lower() == "y":
                os.mkdir(filePath)
                file = open(filePath + "output.txt", 'w+', encoding= 'utf-8')
            else:
                return False
            pass
        
        try:
            file_change = open(filePath + "temp.txt", 'w+', encoding= 'utf-8')
            pass
        except:
            if input(traceback.format_exc() + "\nDo you want to creat a new file in directory:" + filePath + "\nY/N:").lower() == "y":
                os.mkdir(filePath)
                file_change = open(filePath + "temp.txt", 'w+', encoding= 'utf-8')
            else:
                return False
            pass

        file_change.truncate(0)
        while True:
            file_cursor = file.tell()
            data_file = file.readline()
            
            if(Find(data_file, old_line)):
                retext = True
                file_change.write(new_line + "\n")
                # print(new_line)
            else:
                file_change.write(data_file)
                # print(data_file)
                pass

            if(file_cursor == file.tell()):
                if retext == False:
                    file_change.write(new_line + "\n")
                break
        pass
    except:
        print(traceback.format_exc())
        pass
    finally:
        try:
            file.close()
            file_change.close
        except:
            print(traceback.format_exc())
            pass
    
    try:
        file_change = open(filePath + "output.txt", 'w+', encoding= 'utf-8')
        file = open(filePath + "temp.txt", 'r', encoding= 'utf-8')
        file_change.truncate(0)
        while True:
            file_cursor = file.tell()
            data_file = file.readline()
            file_change.write(data_file)
            if(file_cursor == file.tell()):
                break
        pass
    except:
        print(traceback.format_exc())
        pass
    finally:
        try:
            file.close()
            file_change.close
            os.remove(filePath + "temp.txt")
            return True
        except:
            print(traceback.format_exc())
            return False

# Hàm in nội dung ra file. Nếu file nếu chưa tồn tại sẽ được tạo mới. Nếu file đã tồn tại sẽ ghi đè lên nội dung cũ.
def new_file_print(new_line):
    """
    Hàm in nội dung ra file. Nếu file nếu chưa tồn tại sẽ được tạo mới. Nếu file đã tồn tại sẽ ghi đè lên nội dung cũ.
    """
    global filePath
    
    print(new_line)
    
    try:
        
        try:
            file_change = open(filePath + "output.txt", 'w+', encoding= 'utf-8')
            pass
        except:
            if input(traceback.format_exc() + "\nDo you want to creat a new file in directory:" + filePath + "\nY/N:").lower() == "y":
                os.mkdir(filePath)
                file_change = open(filePath + "output.txt", 'w+', encoding= 'utf-8')
            else:
                return False
            pass
        file_change.truncate(0)
        if len(new_line) > 0:
            file_change.write(new_line)
    except:
        print(traceback.format_exc())
        pass
    finally:
        try:
            file_change.close
            return True
        except:
            print(traceback.format_exc())
            return False

# Hàm in nội dung theo list.
def file_list_print(string="", start_string="", list_print=[""], create_new=False):
    """
    Hàm in nội dung list data ra file.
    + string: Chuỗi dữ liệu or dữ liệu muốn thêm vào trước khi in list.
    + list_print: Chuỗi cần in. 
    + start_string: Chuỗi dữ liệu sẽ được in trước mỗi thành phần trong list. 
    """
    if create_new:
        if new_file_print("") == False:
            return False
    try:
        # Print intro listPrint:
        if len(string) > 0 and len(start_string) > 0:
            if file_print(start_string + string) == False:
                return False
        elif len(string) > 0:
            if file_print(string) == False:
                return False
        elif len(start_string) > 0:
            if file_print(start_string) == False:
                return False
        # Print listPrint
        if len(list_print) > 0:
            for item_print in list_print:
                if len(item_print) == 0 or item_print == " ":
                    file_print(start_string + "   -" + "NOTFOUND")
                else:
                    file_print(start_string + "   -" + item_print)
        else:
            file_print(start_string + "   -" + "NOTFOUND")
        file_print("")
        return True
    except:
        print(traceback.format_exc())
        return False

# Hàm lấy dữ liệu từ file .env
def getenv(line_check):
    """
    Hàm lấy dữ liệu từ file ENV
    + Trả về data (str) nếu tìm thấy. 
    + Trả về NOTFOUND nếu không tìm thấy.
    """

    result = "NOTFOUND"
    check_string = False
    try:
        file = open(envPath, 'r+', encoding= 'utf-8')
        while True:
            file_cursor = file.tell()
            data_file = file.readline()
            if check_string == True:
                if len(data_file) > 1:
                    result = result + " " + data_file 
                else:
                    result = result + " " + data_file + "\n"
                for idex in result:
                    if idex == "\"":
                        check_string = False
                        result = result.replace("\"", "")
                        break
            if Find(data_file, line_check):
                if check_string == False:
                    result = data_file.split("=")[1]
                    result = result.split("\n")[0]
                    if Find(result, "\""):
                        check_string = True
                        result = result.replace("\"", "")
                        result = result.replace("\n", "")
                        result = result.replace(" ", "")
                    try:
                        check_roi = result.split(",")
                        for i in range(len(check_roi)):
                            check_roi[i] = int(check_roi[i])
                        if len(check_roi) == 1:
                            result = check_roi[0]
                        break
                    except:
                        if check_string == False:
                            break
            
            if(file_cursor == file.tell()):
                break
        pass
    finally:
        file.close()
    # print("KetQua:", result)
    return result

# Hàm lấy dữ liệu từ file text khác ngoài file mặc định .env. Cách hoạt động tương tự như .env
def get_data_file(line_check, file_path=""):
    """
    Hàm lấy dữ liệu từ file. 
    + line_check: dòng có chứa data cần lấy dữ liệu.
    + file_path: đường dẫn tới file cần đọc.
    """

    result = "NOTFOUND"
    check_string = False
    try:
        file = open(file_path, 'r+', encoding= 'utf-8')
        while True:
            file_cursor = file.tell()
            data_file = file.readline()
            if check_string == True:
                if len(data_file) > 1:
                    result = result + " " + data_file 
                else:
                    result = result + " " + data_file + "\n"
                for idex in result:
                    if idex == "\"":
                        check_string = False
                        result = result.replace("\"", "")
                        break
            if Find(data_file, line_check):
                if check_string == False:
                    result = data_file.split("=")[1]
                    if Find(result, "\""):
                        check_string = True
                        result = result.replace("\"", "")
                        # result = result.replace("\n", "")
                        result = result.replace(" ", "")
                    else:
                        result = result.split("\n")[0]
                    try:
                        check_roi = result.split(",")
                        for i in range(len(check_roi)):
                            check_roi[i] = int(check_roi[i])
                        if len(check_roi) == 1:
                            result = check_roi[0]
                        break
                    except:
                        if check_string == False:
                            break
            
            if(file_cursor == file.tell()):
                break
        pass
    finally:
        file.close()
    # print("KetQua:", result)
    return result

# Hàm lấy tất cả các dòng dữ liệu có nội dung cần lấy trong file .env
def get_all_env(line_check):
    """
    Hàm lấy dữ liệu từ file ENV
    + Trả về data (str) nếu tìm thấy. 
    + Trả về NOTFOUND nếu không tìm thấy.
    """

    result = "NOTFOUND"
    list_result = []
    check_string = False
    try:
        try:
            file = open(envPath, 'r+', encoding= 'utf-8')
        except:
            if input(traceback.format_exc() + "\nDo you want to creat a new file in directory:" + envPath + "\nY/N:").lower() == "y":
                # os.mkdir(filePath)
                file = open(envPath, 'w+', encoding= 'utf-8')
            else:
                return list_result
            pass
        while True:
            file_cursor = file.tell()
            data_file = file.readline()
            if check_string == True:
                if len(data_file) > 1:
                    result = result + " " + data_file 
                else:
                    result = result + " " + data_file + "\n"
                for idex in result:
                    if idex == "\"":
                        check_string = False
                        result = result.replace("\"", "")
                        break
            if Find(data_file, line_check):
                if check_string == False:
                    result = data_file.split("=")[1]
                    result = result.split("\n")[0]
                    if Find(result, "\""):
                        check_string = True
                        result = result.replace("\"", "")
                        result = result.replace("\n", "")
                        result = result.replace(" ", "")
                        list_result.append(result)
                    try:
                        if check_string == False:
                            check_roi = result.split(",")
                            for i in range(len(check_roi)):
                                check_roi[i] = int(check_roi[i])
                            if len(check_roi) == 1:
                                result = check_roi[0]
                                list_result.append(result)
                    except:
                        if check_string == False:
                            break
            
            if(file_cursor == file.tell()):
                break
        pass
    finally:
        file.close()
    # print("KetQua:", result)
    return list_result

def SaveJson(path=jsonPath, data={}):
    try:
        with open(path, "w") as json_file:
            dump(data, json_file, indent=4)
            json_file.close()
    except:
        print(traceback.format_exc())
    pass

def GetJson(path=jsonPath):
    try:
        # Đọc file json:
        with open(path) as json_file:
            data = load(json_file)
            json_file.close()
        return data
    except:
        print(traceback.format_exc())
        return {}
        pass