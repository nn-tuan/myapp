import traceback

try:
    import sys
    from tkinter import *
    from cefpython3 import cefpython as cef
    import ctypes
except:
    print(traceback.format_exc())
    input("Error!")

# Class tạo ra Frame để nhúng Browser vào
class MainFrame(Frame):
    def __init__(self, root, http_url):
        assert cef.__version__ >= "55.3", "CEF Python v55.3+ required to run this"
        sys.excepthook = cef.ExceptHook 
        
        self.first_run = False
        self.browser_frame = None
        self.navigation_bar = None
        self.root = root
        self.url = http_url
        self.creat_window()
        self.close = False
        self.run_after()

    def creat_window(self):
        # MainFrame
        Frame.__init__(self, master=self.root)
        # self.master.protocol("WM_DELETE_WINDOW", self.on_close)

        # BrowserFrame
        self.browser_frame = BrowserFrame(self, self.navigation_bar, self.url)
        self.browser_frame.grid(row=1, column=0,
                                sticky=(N + S + E + W))
        Grid.rowconfigure(self, 1, weight=1)
        Grid.columnconfigure(self, 0, weight=1)

        # Pack MainFrame
        self.pack(fill=BOTH, expand=YES)

    def on_close(self):
        if self.browser_frame:
            self.browser_frame.on_root_close()
            self.browser_frame = None
        else:
            self.master.destroy()

    def run_after(self):
        if self.close:
            self.on_close()
        else:
            try:
                self.root.after(100, self.run_after)
            except:
                print(traceback.format_exc())

# Class tạo ra Browser
class BrowserFrame(Frame):
    def __init__(self, mainframe, navigation_bar=None, http_url="google.com"):
        self.navigation_bar = navigation_bar
        self.browser = None
        self.url = http_url
        Frame.__init__(self, mainframe)
        self.mainframe = mainframe
        self.bind("<FocusIn>", self.on_focus_in)
        self.bind("<Configure>", self.on_configure)
        self.focus_set()

    def embed_browser(self):
        window_info = cef.WindowInfo()
        rect = [0, 0, self.winfo_width(), self.winfo_height()]
        window_info.SetAsChild(self.get_window_handle(), rect)
        
        # print(self.url)
        self.browser = cef.CreateBrowserSync(window_info, url=self.url)
        assert self.browser
        self.browser.SetClientHandler(LifespanHandler(self))
        self.browser.SetClientHandler(LoadHandler(self))
        self.browser.SetClientHandler(FocusHandler(self))
        self.message_loop_work()

    def get_window_handle(self):
        if self.winfo_id() > 0:
            return self.winfo_id()
        else:
            raise Exception("Couldn't obtain window handle")

    def message_loop_work(self):
        try:
            cef.MessageLoopWork()
            self.after(25, self.message_loop_work)
        except:
            try: 
                self.destroy()
            except:
                print("Error code 0001", traceback.format_exc())

    def on_configure(self, _):
        if not self.browser:
            self.embed_browser()

    def on_root_configure(self):
        # Root <Configure> event will be called when top window is moved
        if self.browser:
            self.browser.NotifyMoveOrResizeStarted()

    def on_mainframe_configure(self, width, height):
        if self.browser:
            ctypes.windll.user32.SetWindowPos(
                self.browser.GetWindowHandle(), 0,
                0, 0, width, height, 0x0002)
            self.browser.NotifyMoveOrResizeStarted()

    def on_focus_in(self, _):
        if self.browser:
            self.browser.SetFocus(True)
            
    def clear_browser_references(self):
        # Clear browser references that you keep anywhere in your
        # code. All references must be cleared for CEF to shutdown cleanly.
        self.browser = None

    def on_root_close(self):
        try:
            if self.browser:
                self.browser.CloseBrowser(True)
                self.clear_browser_references()
            else:
                self.destroy()
        except:
            print(traceback.format_exc())

class LifespanHandler(object):

    def __init__(self, tkFrame):
        self.tkFrame = tkFrame

    def OnBeforeClose(self, browser, **_):
        self.tkFrame.quit()

class LoadHandler(object):

    def __init__(self, browser_frame):
        self.browser_frame = browser_frame

    def OnLoadStart(self, browser, **_):
        if self.browser_frame.master.navigation_bar:
            self.browser_frame.master.navigation_bar.set_url(browser.GetUrl())

class FocusHandler(object):
    """For focus problems see Issue #255 and Issue #535. """

    def __init__(self, browser_frame):
        self.browser_frame = browser_frame

    def OnTakeFocus(self, next_component, **_):
        # root_log.debug("FocusHandler.OnTakeFocus, next={next}"
                    #  .format(next=next_component))
        pass

    def OnSetFocus(self, source, **_):
        return True

    def OnGotFocus(self, **_):
        # root_log.debug("FocusHandler.OnGotFocus")
        pass

class Tabs(Frame):

    def __init__(self):
        Frame.__init__(self)
        # TODO: implement tabs
