from select import select
import traceback
try:
    import time
    import tkinter as TK
    from threading import Thread

    import pyautogui as pya
    import pyperclip

    from Model.Browser import *
    import Model.FileProcess as File
    from Model.InputModel import Input
except:
    print(traceback.format_exc())
    pass
#===========================================================================================#
# Class khởi tạo màn hình Dictionary lúc Search:
#===========================================================================================#
class Dictionary_Search():
    def __init__(self):
        assert cef.__version__ >= "55.3", "CEF Python v55.3+ required to run this"
        sys.excepthook = cef.ExceptHook 
        self.master = TK.Tk()
        self.is_first_run = True
        self.url = "https://www.google.com"
        self.position = (0, 0)
        self.creat_window()
        pass

    def update(self, event):
        self.position = (self.master.winfo_x(), self.master.winfo_y())
    # Hàm khởi tạo màn hình Search Dictionary
    def creat_window(self):
        self.check = True
        self.master.geometry("640x720")
        self.master.title("Dictionary")
        self.master.resizable(False, False)
        self.master.bind('<Configure>', self.update)
        self.center()
        self.hide()
        
        self.run()
        self.master.protocol("WM_DELETE_WINDOW", self.hide)
    
    # Di chuyển màn hình đến vị trí X, Y
    def move_window(self, width=0, height=0, x=0, y=0):
        if width == 0:
            width = self.master.winfo_width()
        if height == 0:
            height = self.master.winfo_height()
        
        frm_width = self.master.winfo_rootx() - self.master.winfo_x()
        win_width = width + 2 * frm_width

        titlebar_height = self.master.winfo_rooty() - self.master.winfo_y()
        win_height = height + titlebar_height + frm_width

        x_check = self.master.winfo_screenwidth() - x - win_width
        y_check = self.master.winfo_screenheight() - y - win_height
        # if x_check <= 0:
        #     x += x_check
        if y_check <= 0:
            y += y_check

        self.master.geometry('{}x{}+{}+{}'.format(width, height, x, y))
        self.master.deiconify()
        self.position = (x, y)
        pass

    # Chỉnh màn hình app vào chính giữa màn hình PC
    def center(self):
        self.master.update_idletasks()

        width = self.master.winfo_width()
        frm_width = self.master.winfo_rootx() - self.master.winfo_x()
        win_width = width + 2 * frm_width

        height = self.master.winfo_height()
        titlebar_height = self.master.winfo_rooty() - self.master.winfo_y()
        win_height = height + titlebar_height + frm_width

        x = self.master.winfo_screenwidth() // 2 - win_width // 2
        y = self.master.winfo_screenheight() // 2 - win_height // 2
        self.master.geometry('{}x{}+{}+{}'.format(width, height, x, y))
        self.master.deiconify()
        self.position = (x, y)

    # Hàm set giá trị URL
    def set(self, url):
        self.url = url
        pass

    # Hàm chạy URL nhập mới vào:
    def LoadUrl(self, url):
        try:
            if self.is_first_run:
                self.app = MainFrame(self.master, url)
                pass
            else:
                self.app.browser_frame.browser.LoadUrl(url)
        except:
            print(traceback.format_exc())

    # Hiển thị window
    def show(self):
        if self.is_first_run:
            self.is_first_run = False
        try:
            self.master.deiconify()
        except:
            print(traceback.format_exc())
    
    # Ẩn Window
    def hide(self):
        try:
            self.master.withdraw()
        except:
            print(traceback.format_exc())

    # Hàm gọi màn hình Search Dictionary khởi động
    def run(self):
        try:
            self.settings = {}
            cef.Initialize(settings=self.settings)
        except:
            print(traceback.format_exc())

    # Hàm tắt màn hình Search Dictionary
    def stop(self):
        try:
            # self.app.on_close()
            self.app.close = True
        except:
            pass



#===========================================================================================#
# Class khởi tạo chương trình chạy, điều khiển Dictionary:
#===========================================================================================#
class Dictionary_Run(Thread):
    def __init__(self, key="Key.ctrl_l", File=File):
        Thread.__init__(self)
        self.key = key

        self.runing = False
        self.FP = File
        self.check_key = False
        self.start_record_key = False
        self.check_mouse = False
        self.mouse_key_input = Input()
        self.time = 0

        self.position=(0, 0)
        pass

    # Hàm trả về dữ liệu copy từ clipboard:
    def copy_clipboard(self):
        try:
            pyperclip.copy("") # <- This prevents last copy replacing current copy of null.
            pya.hotkey('ctrl', 'c')
            time.sleep(0.3)  # ctrl-c is usually very fast but your program may execute faster 
            return str(pyperclip.paste())
        except:
            print(traceback.format_exc())
            return ""

    # Hàm set giá trị URL của thư viện và data cần thiết để thư viện tìm kiếm.
    def SetUrl(self, data, value):
        self.url = data
        self.value = value

    def Check_Mouse(self):
        if self.mouse_key_input.mouse_left_click:
            self.check_mouse = True
            self.time = time.time()
            self.position = self.mouse_key_input.Get_Position()
            if self.position[0] < self.dictionary.position[0] or self.position[0] > self.dictionary.position[0] + self.dictionary.master.winfo_width() or self.position[1] < self.dictionary.position[1] or self.position[1] > self.dictionary.position[1] + self.dictionary.master.winfo_height():
                self.dictionary.hide()
            pass
            pass
        else:
            if self.check_mouse and time.time() - self.time > 1:
                self.check_mouse = False

    def Check_Key(self):
        if len(self.mouse_key_input.list_key) == 1:
            if self.mouse_key_input.list_key[0] == self.key and self.check_key == False and self.start_record_key == False and self.check_mouse:
                self.check_key = True
        elif len(self.mouse_key_input.list_key) == 0:
            if self.runing and self.check_key and self.start_record_key == False:
                self.Dictionary_Run()
                print("test")
            self.check_key = False
            self.start_record_key = False
        else:
            self.start_record_key = True
            self.check_key = False

    # Hàm thực thi kiểm tra Dictionary:
    def run_after(self):
        self.Check_Mouse()
        self.Check_Key() 
        self.dictionary.master.after(10, self.run_after)

    # Hàm chạy Thread để kiểm tra trạng thái hiển thị của Dictionary.
    def run(self):
        self.runing = True
        self.mouse_key_input.start()
        self.dictionary = Dictionary_Search()
        self.dictionary.master.after(10, self.run_after)
        self.dictionary.master.mainloop()
    
    # Chuyển trạng thái dừng hoạt động hiển thị của Dictionaray.
    def stop(self):
        self.runing = False
        print("Pause")
    
    # Chuyển trạng thái tiếp tục hoạt động của Dictionary.
    def continues(self):
        self.runing = True
        print("Continue")

     # Hàm chuyển trạng thái của Dictionary thành Run, truyền thông số của thư viện đã lưu cho Dictionary.
    def Dictionary_Run(self):
        # if self.runing != True:
        with open(self.FP.jsonPath, "r+") as json_file:
            data = self.FP.load(json_file)
            json_file.close()
        self.url = data['Dictonary'][data['Select']]['website']
        self.value = data['Dictonary'][data['Select']]['value']
        
        try:
            url = self.url.split(self.value)[0] + self.copy_clipboard() + self.url.split(self.value)[1]
            self.dictionary.LoadUrl(url)
        except:
            url = self.url.split(self.value)[0] + self.copy_clipboard()
            self.dictionary.LoadUrl(url)
        
        self.dictionary.show()
        self.dictionary.move_window(x=self.position[0], y=self.position[1])
            

    # Chương trình xử lý sự kiện Start để chạy hiển thị thư viện.
    def StartDictionary(self, event=None):
        try:
            if(self.is_alive()):
                if self.runing:
                    pass
                else:
                    self.continues()
                pass
            else:
                self.start()
            pass
        except:
            print(traceback.format_exc())
        # print("test")
        pass
    
    # Chương trình xử lý không hiển thị thư viện khi chạy.
    def StopDictionary(self):
        if self.runing:
            self.dictionary.hide()
            self.stop()