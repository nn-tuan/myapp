import traceback

try:
    import os, sys

    from tkinter import *
    from ViewModel.MainCtrlViewModel import MainModel
except:
    print(traceback.format_exc())
    input("Error!\nPress any key to exit!")


def main():
    try:     
        MW = MainModel()
        MW.MainWindows()
        os._exit(1)
    except:
        print(traceback.format_exc())
        input("Error!\nPress any key to exit!")


if __name__ == "__main__":
    main()