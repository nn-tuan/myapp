import shutil
import os
import traceback
 
# Get the list of all files and directories
path = os.getcwd()
scr = path + "\\build\DictionaryApp\\"
dic = path + "\\dist\DictionaryApp"
dir_list = os.listdir(scr)


max = 0
for file in dir_list:
    if max < len(scr + file):
        max = len(scr + file)
# 2nd option
check_error = []
for file in dir_list:
    try:
        shutil.copy2(scr + file, dic,follow_symlinks=True) 
        print(scr + file, (max - len(scr + file)) * " " + "========>", dic, ": OK")
    except:
        try:
            shutil.copytree(scr + file, dic)
        except:
            check_error.append(scr + file)

print(160 * "=")
if len(check_error) > 0:
    for error_item in check_error:
        print(error_item, (max - len(error_item)) * " " + "========>", "Error, please try again")
        
    print("Error!")
else:
    print("Done!")